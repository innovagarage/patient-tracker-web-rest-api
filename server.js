var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;
var MongoClient = mongodb.MongoClient;
var db_url = "mongodb://kerembalcan:123456@ds047782.mongolab.com:47782/restdeneme";
var port = process.env.PORT || 3000;

var BILEKLIKAPP_COLLECTION = "BileklikApp";

var app = express();
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

var db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(db_url, function (err, database) {
 if (err) {
   console.log(err);
   process.exit(1);
 }

 // Save database object from the callback for reuse.
 db = database;

 console.log("DB'ye Baglandik!!!");
 var collectionApp = database.collection('BileklikApp');


 // Initialize the app.
 var server = app.listen(port, function () {

   console.log("Server su port uzerinde calisiyor", port);
 });

});

// CONTACTS API ROUTES BELOW



function handleError(reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}


app.get("/bileklikapp", function(req, res) {
  db.collection(BILEKLIKAPP_COLLECTION).find({}).toArray(function(err, docs) {
    if (err) {
      handleError(err.message, "Failed to get data.");
    } else {
      res.status(200).json(docs);
      console.log(docs[0]._id + " " + docs[0]._name);
    }
  });
});

app.get("/bileklikapp/:id", function(req, res) {
  db.collection(BILEKLIKAPP_COLLECTION).findOne({ _id: ObjectID(req.params.id) }, function(err, doc) {
    if (err) {
      handleError(err.message, "Failed to get data");
    } else {
      res.status(200).json(doc);
    }
  });
});

app.post("/bileklikapp", function(req, res) {
  var bileklikapp = req.body;

  db.collection(BILEKLIKAPP_COLLECTION).insertOne(bileklikapp, function(err, doc) {
    if (err) {
      handleError(err.message, "Failed to create new data.");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});

app.put("/bileklikapp/:id", function(req, res) {
  var updateDoc = req.body;

  db.collection(BILEKLIKAPP_COLLECTION).updateOne({_id: new ObjectID(req.params.id)}, updateDoc, function(err, doc) {
    if (err) {
      handleError(err.message, "Failed to update contact");
    } else {
      res.status(204).end();
    }
  });
});

app.delete("/bileklikapp/:id", function(req, res) {
  db.collection(BILEKLIKAPP_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(err.message, "Failed to delete contact");
    } else {
      res.status(204).end();
    }
  });
});
