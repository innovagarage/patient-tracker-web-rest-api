angular.module("bileklikApp", ['ngRoute'])
    .config(function($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "list.html",
                controller: "ListController",
                resolve: {
                    contacts: function(Contacts) {
                        return Contacts.getContacts();
                    }
                }
            })
            .when("/details/:id", {
                templateUrl: "details.html",
                controller: "DetailsContactController"

            })
            .otherwise({
                redirectTo: "/"
            })
    })
    .service("Contacts", function($http) {
        this.getContacts = function() {
            return $http.get("/bileklikapp").
                then(function(response) {
                    return response;
                }, function(response) {
                    alert("Error finding data.");
                });
        }
        this.getContact = function(id) {
            var url = "/bileklikapp/" + id;
            console.log(url);
            return $http.get(url).
                then(function(response) {
                    return response;
                }, function(response) {
                    alert("Error finding this data.");
                });
        }

    })
    .controller("ListController", function(contacts, $scope) {
        $scope.bileklikapps = contacts.data;
    })

    .controller("DetailsContactController", function($scope, $routeParams, Contacts) {
        Contacts.getContact($routeParams.id).then(function(doc) {
            $scope.bileklikapp = doc.data;
        }, function(response) {
            alert(response);
        });

    });
